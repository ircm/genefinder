Gene Finder
====================

Finds genes based on a list of protein ids.

## Repo moved to github
https://github.com/benoitcoulombelab/genefinder
